package org.jc.solutions.design.patterns.structural.adapter;

class MacbookPro implements Laptop {
	private String productName;
	private String screenSize;
	private String manufacturer;
	private double batteryWh;
	
	public MacbookPro(String productName, String screenSize, String manufacturer, double batteryWh) {
		super();
		this.productName = productName;
		this.screenSize = screenSize;
		this.manufacturer = manufacturer;
		this.batteryWh = batteryWh;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getScreenSize() {
		return screenSize;
	}
	
	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public double getBatteryWh() {
		return batteryWh;
	}
	
	public void setBatteryMAh(double batteryWh) {
		this.batteryWh = batteryWh;
	}
	
	public String toString() {
		return "MacbookPro [productName=" + productName + ", screenSize=" + screenSize + ", manufacturer="
				+ manufacturer + ", batteryWh=" + batteryWh + "]";
	}
}
