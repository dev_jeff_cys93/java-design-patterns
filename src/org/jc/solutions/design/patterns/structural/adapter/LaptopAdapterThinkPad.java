package org.jc.solutions.design.patterns.structural.adapter;

public class LaptopAdapterThinkPad implements Laptop {
	
	private ThinkPad instance;
	
	public LaptopAdapterThinkPad(ThinkPad instance) {
		this.instance = instance;
	}

	@Override
	public String getProductName() {
		return instance.getName();
	}

	@Override
	public String getScreenSize() {
		return instance.getSize();
	}

	@Override
	public String getManufacturer() {
		return instance.getBrand();
	}

	@Override
	public double getBatteryWh() {
		return instance.getBatteryWh();
	}

	public String toString() {
		return "ThinkPad [productName=" + getProductName() + ", screenSize=" + getScreenSize() + ", manufacturer="
				+ getManufacturer() + ", batteryWh=" + getBatteryWh() + "]";
	}
}
