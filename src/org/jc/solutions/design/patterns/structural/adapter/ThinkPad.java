package org.jc.solutions.design.patterns.structural.adapter;

class ThinkPad {
	private String name;
	private String size;
	private String brand;
	private double batteryWh;
	
	public ThinkPad(String name, String size, String brand, double batteryWh) {
		this.name = name;
		this.size = size;
		this.brand = brand;
		this.batteryWh = batteryWh;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSize() {
		return size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public double getBatteryWh() {
		return batteryWh;
	}
	
	public void setBatteryWh(double batteryWh) {
		this.batteryWh = batteryWh;
	}
}
