package org.jc.solutions.design.patterns.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class AdapterMain {
	public static void main(String[] args) {
		List<Laptop> laptops = new ArrayList<>();
		
		Laptop macbookPro = new MacbookPro("Macbook Pro 16-inch", "16 inches", "Apple", 100);
		laptops.add(macbookPro);
				
		Laptop thinkPad = new LaptopAdapterThinkPad(new ThinkPad("ThinkPad X1 Extreme Gen 2", "Lenovo", "15.6 inches", 80));
		laptops.add(thinkPad);
		
		Laptop zephyrus = new LaptopAdapterZephyrus(new Zephyrus("ROG Zephyrus S (GX531)", 15.6, "Asus", 60));
		laptops.add(zephyrus);
		
		for(Laptop laptop: laptops) {
			System.out.println(laptop);
		}
	}
}
