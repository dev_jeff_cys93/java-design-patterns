package org.jc.solutions.design.patterns.structural.adapter;

public class LaptopAdapterZephyrus implements Laptop {
	
	private Zephyrus instance;
	
	public LaptopAdapterZephyrus(Zephyrus instance) {
		this.instance = instance;
	}

	@Override
	public String getProductName() {
		return instance.getLaptopName();
	}

	@Override
	public String getScreenSize() {
		return instance.getSize() + " inches";
	}

	@Override
	public String getManufacturer() {
		return instance.getBrand();
	}

	@Override
	public double getBatteryWh() {
		return instance.getBatteryWh();
	}
	
	public String toString() {
		return "Zephyrus [productName=" + getProductName() + ", screenSize=" + getScreenSize() + ", manufacturer="
				+ getManufacturer() + ", batteryWh=" + getBatteryWh() + "]";
	}
}
