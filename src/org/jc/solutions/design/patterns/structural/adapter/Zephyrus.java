package org.jc.solutions.design.patterns.structural.adapter;

public class Zephyrus {
	private String laptopName;
	private double size;
	private String brand;
	private double batteryWh;
	
	public Zephyrus(String laptopName, double size, String brand, double batteryWh) {
		this.laptopName = laptopName;
		this.size = size;
		this.brand = brand;
		this.batteryWh = batteryWh;
	}

	public String getLaptopName() {
		return laptopName;
	}

	public void setLaptopName(String laptopName) {
		this.laptopName = laptopName;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getBatteryWh() {
		return batteryWh;
	}

	public void setBatteryWh(double batteryWh) {
		this.batteryWh = batteryWh;
	}
}
