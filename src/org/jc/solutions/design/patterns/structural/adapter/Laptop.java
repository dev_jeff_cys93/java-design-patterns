package org.jc.solutions.design.patterns.structural.adapter;

interface Laptop {
	String getProductName();
	String getScreenSize();
	String getManufacturer();
	double getBatteryWh();
	String toString();
}
