package org.jc.solutions.design.patterns.structural.decorator;

interface CarSpec {
    String getSpec();
}
