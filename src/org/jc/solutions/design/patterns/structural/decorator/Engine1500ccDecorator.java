package org.jc.solutions.design.patterns.structural.decorator;

class Engine1500ccDecorator extends CarSpecDecorator {
    private static String ENGINE_TECH = "16-valve DOHC, turbocharged with variable valve timing";
    private static String ENGINE_DISPLACEMENT = "1,498 cc";
    private static String ENGINE_POWER = "171 hp at 5,500 rpm";
    private static String ENGINE_TORQUE = "220 Nm at 1,700 rpm";
    public Engine1500ccDecorator(CarSpec carSpec) {
        super(carSpec);
    }

    public String getSpec() {
        return super.getSpec() + getEngineSpec();
    }

    public String getEngineSpec() {
        String engineSpec = "\nEngine Tech:\t%s\nDisplacement:\t%s\nPower:\t%s\nTorque:\t%s";
        return String.format(engineSpec, ENGINE_TECH, ENGINE_DISPLACEMENT, ENGINE_POWER, ENGINE_TORQUE);
    }
}
