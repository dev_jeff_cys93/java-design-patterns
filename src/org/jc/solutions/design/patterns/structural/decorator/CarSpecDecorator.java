package org.jc.solutions.design.patterns.structural.decorator;

abstract class CarSpecDecorator implements CarSpec {
    protected CarSpec carSpec;

    public CarSpecDecorator(CarSpec carSpec) {
        this.carSpec = carSpec;
    }

    @Override
    public String getSpec() {
        return carSpec.getSpec();
    }
}
