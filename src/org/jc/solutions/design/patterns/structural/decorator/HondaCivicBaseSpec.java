package org.jc.solutions.design.patterns.structural.decorator;

class HondaCivicBaseSpec implements CarSpec {
    private static final String CAR_LENGTH = "4,648 mm";
    private static final String CAR_WIDTH = "1,799 mm";
    private static final String CAR_HEIGHT = "1,416 mm";
    private static final String CAR_WHEELBASE = "2,700 mm";
    private static final String CAR_FUEL_TANK = "47 litres";

    private String modelName;

    public HondaCivicBaseSpec(String modelName) {
        this.modelName = modelName;
    }

    @Override
    public String getSpec() {
        String carSpec = "Honda Civic %s\nLength:\t%s\nWidth:\t%s\nHeight:\t%s\nWheelbase:\t%s\nFuel Tank:\t%s";
        return String.format(carSpec, modelName, CAR_LENGTH, CAR_WIDTH, CAR_HEIGHT, CAR_WHEELBASE, CAR_FUEL_TANK);
    }
}
