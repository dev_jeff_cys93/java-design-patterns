package org.jc.solutions.design.patterns.structural.decorator;

class HeadLampHalogenDecorator extends CarSpecDecorator {
    private static String HEAD_LAMP = "Halogen Projector";
    public HeadLampHalogenDecorator(CarSpec carSpec) {
        super(carSpec);
    }

    @Override
    public String getSpec() {
        return super.getSpec();
    }

    public String getHeadLamp() {
        return "\nHead Lamp:\t" + HEAD_LAMP;
    }
}
