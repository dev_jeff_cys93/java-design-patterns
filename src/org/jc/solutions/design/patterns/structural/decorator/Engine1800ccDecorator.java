package org.jc.solutions.design.patterns.structural.decorator;

class Engine1800ccDecorator extends CarSpecDecorator {
    private static String ENGINE_TECH = "16-valve DOHC, turbocharged with variable valve timing";
    private static String ENGINE_DISPLACEMENT = "1,799 cc";
    private static String ENGINE_POWER = "141 hp at 6,500 rpm";
    private static String ENGINE_TORQUE = "174 Nm at 4,300 rpm";
    public Engine1800ccDecorator(CarSpec carSpec) {
        super(carSpec);
    }

    public String getSpec() {
        return super.getSpec() + getEngineSpec();
    }

    public String getEngineSpec() {
        String engineSpec = "\nEngine Tech:\t%s\nDisplacement:\t%s\nPower:\t%s\nTorque:\t%s";
        return String.format(engineSpec, ENGINE_TECH, ENGINE_DISPLACEMENT, ENGINE_POWER, ENGINE_TORQUE);
    }
}
