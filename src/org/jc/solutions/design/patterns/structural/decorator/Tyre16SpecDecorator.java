package org.jc.solutions.design.patterns.structural.decorator;

class Tyre16SpecDecorator extends CarSpecDecorator {
    private static String TYRE_SIZE = "215/55R16";
    public Tyre16SpecDecorator(CarSpec carSpec) {
        super(carSpec);
    }

    public String getSpec() {
        return super.getSpec() + getTyreSpec();
    }

    public String getTyreSpec() {
        String typeSpec = "\nFront Tyre:\t%s\nRear Tyre:\t%s";
        return String.format(typeSpec, TYRE_SIZE, TYRE_SIZE);
    }
}
