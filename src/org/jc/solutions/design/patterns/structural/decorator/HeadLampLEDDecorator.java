package org.jc.solutions.design.patterns.structural.decorator;

class HeadLampLEDDecorator extends CarSpecDecorator {
    private static String HEAD_LAMP = "LED Projector";
    public HeadLampLEDDecorator(CarSpec carSpec) {
        super(carSpec);
    }

    @Override
    public String getSpec() {
        return super.getSpec();
    }

    public String getHeadLamp() {
        return "\nHead Lamp:\t" + HEAD_LAMP;
    }
}
