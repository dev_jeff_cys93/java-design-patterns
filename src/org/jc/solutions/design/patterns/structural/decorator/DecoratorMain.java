package org.jc.solutions.design.patterns.structural.decorator;

public class DecoratorMain {
    public static void main(String[] args) {
        CarSpec hondaCivic = new Engine1800ccDecorator(new HeadLampHalogenDecorator(new Tyre16SpecDecorator(new HondaCivicBaseSpec("1.8"))));
        CarSpec hondaCivicTC = new Engine1500ccDecorator(new HeadLampLEDDecorator(new Tyre17SpecDecorator(new HondaCivicBaseSpec("1.5 TC"))));
        CarSpec hondaCivicTP = new Engine1500ccDecorator(new HeadLampLEDDecorator(new Tyre18SpecDecorator(new HondaCivicBaseSpec("1.5 TCP"))));

        System.out.println(hondaCivic.getSpec() + "\n");

        System.out.println(hondaCivicTC.getSpec() + "\n");

        System.out.println(hondaCivicTP.getSpec() + "\n");
    }
}

