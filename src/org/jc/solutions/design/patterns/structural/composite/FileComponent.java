package org.jc.solutions.design.patterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

abstract class FileComponent {
    protected String name;
    protected List<FileComponent> files = new ArrayList<>();

    void addFile(FileComponent fileComponent) {
        throw new UnsupportedOperationException();
    }

    public String getName() {
        return name;
    }

    abstract String printFilePath();
}
