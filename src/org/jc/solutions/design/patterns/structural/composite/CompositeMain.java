package org.jc.solutions.design.patterns.structural.composite;

public class CompositeMain {
    public static void main(String[] args) {
        FileComponent homeFiles = new Directory("HOME");

        FileComponent directoryA = new Directory("A");
        FileComponent fileD = new File("D");
        FileComponent fileE = new File("E");

        FileComponent directoryB = new Directory("B");
        FileComponent directoryF = new Directory("F");
        FileComponent fileG = new File("G");
        FileComponent fileH = new File("H");

        FileComponent fileC = new File("C");

        directoryA.addFile(fileD);
        directoryA.addFile(fileE);

        directoryF.addFile(fileG);
        directoryF.addFile(fileH);
        directoryB.addFile(directoryF);

        homeFiles.addFile(directoryA);
        homeFiles.addFile(directoryB);
        homeFiles.addFile(fileC);

        System.out.print(homeFiles.printFilePath());
    }
}
