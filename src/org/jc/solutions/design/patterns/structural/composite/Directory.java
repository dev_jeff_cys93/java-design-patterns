package org.jc.solutions.design.patterns.structural.composite;

import java.util.Iterator;

class Directory extends FileComponent {
    public Directory(String name) {
        this.name = name;
    }

    @Override
    void addFile(FileComponent fileComponent) {
        files.add(fileComponent);
    }

    @Override
    String printFilePath() {
        return printFilePath(1);
    }

    String printFilePath(int fileLevel) {
        StringBuilder sb = new StringBuilder();
        Iterator<FileComponent> it = files.iterator();
        sb.append(name);

        while (it.hasNext()) {
            FileComponent file = it.next();
            sb.append("\n");
            for(int i = 0; i < fileLevel; i++)
                sb.append("\t");
            sb.append(" - ");
            if (file instanceof Directory) {
                Directory directory = (Directory) file;
                sb.append(directory.printFilePath(fileLevel + 1));
            } else {
                sb.append(file.printFilePath());
            }
        }
        return sb.toString();
    }
}
