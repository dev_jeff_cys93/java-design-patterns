package org.jc.solutions.design.patterns.structural.composite;

class File extends FileComponent {
    public File(String name) {
        this.name = name;
    }

    @Override
    String printFilePath() {
        return name;
    }
}
