package org.jc.solutions.design.patterns.structural.flyweight;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class TextConsole {
    private final CharacterPool characterPool = new CharacterPool();
    private final List<CharacterRegistry> characterRegistries = new CopyOnWriteArrayList<>();

    public void insert(String inputText) {
        String[] characters = inputText.split("");
        for (String character: characters) {
            CharacterRegistry characterRegistry = characterPool.lookup(character);
            characterRegistries.add(characterRegistry);
        }
    }

    public void print() {
        for (CharacterRegistry characterRegistry: characterRegistries) {
            System.out.print(characterRegistry.getCharacter());
            characterRegistries.remove(characterRegistry);
        }
    }

    public void printStoredCharacters() {
        System.out.println("\nTotal characters stored: " + characterPool.totalCharacterStored());
        characterPool.printAllCharacterRegistries();
    }
}
