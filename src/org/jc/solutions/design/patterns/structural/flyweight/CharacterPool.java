package org.jc.solutions.design.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

class CharacterPool {
    private Map<String, CharacterRegistry> characterMap = new HashMap<>();

    public CharacterRegistry lookup(String character) {
        if(!characterMap.containsKey(character)) {
            characterMap.put(character, new CharacterRegistry(character));
        }
        return characterMap.get(character);
    }

    public int totalCharacterStored() {
        return characterMap.size();
    }

    public void printAllCharacterRegistries() {
        for (CharacterRegistry characterRegistry: characterMap.values()) {
            System.out.println(characterRegistry.toString());
        }
    }
}
