package org.jc.solutions.design.patterns.structural.flyweight;

class CharacterRegistry {
    private final String character;
    private final String memory;

    private static Integer memoryCounter = 1;

    private static String getMemoryAddress() {
        final Integer MAX_LENGTH = 5;
        final String PREFIX = "X";
        final String ZERO = "0";
        StringBuilder memoryAddress = new StringBuilder(PREFIX);
        String counter = String.valueOf(memoryCounter++);
        for (int i = 1; i < MAX_LENGTH - counter.length(); i++) {
            memoryAddress.append(ZERO);
        }
        memoryAddress.append(counter);
        return memoryAddress.toString();
    }

    public CharacterRegistry(String character) {
        this.character = character;
        this.memory = getMemoryAddress();
    }

    public String getCharacter() {
        return character;
    }

    public String getMemory() {
        return memory;
    }

    @Override
    public String toString() {
        return String.format("%s[%s]", character, memory);
    }
}
