package org.jc.solutions.design.patterns.structural.facade;

class HondaCRV implements Car {
    @Override
    public void carName() {
        System.out.println("Honda CRV");
    }
}
