package org.jc.solutions.design.patterns.structural.facade;

class HondaCarPrinter {
    private HondaCity hondaCity;
    private HondaJazz hondaJazz;
    private HondaCivic hondaCivic;
    private HondaBRV hondaBRV;
    private HondaHRV hondaHRV;
    private HondaCRV hondaCRV;

    public HondaCarPrinter() {
        this.hondaCity = new HondaCity();
        this.hondaJazz = new HondaJazz();
        this.hondaCivic = new HondaCivic();
        this.hondaBRV = new HondaBRV();
        this.hondaHRV = new HondaHRV();
        this.hondaCRV = new HondaCRV();
    }

    void printCity() { hondaCity.carName(); }
    void printJazz() { hondaJazz.carName(); }
    void printCivic() { hondaCivic.carName(); }
    void printBRV() { hondaBRV.carName(); }
    void printHRV() { hondaHRV.carName(); }
    void printCRV() { hondaCRV.carName(); }
}
