package org.jc.solutions.design.patterns.structural.facade;

interface Car {
    void carName();
}
