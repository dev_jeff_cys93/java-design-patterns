package org.jc.solutions.design.patterns.structural.facade;

class HondaHRV implements Car {
    @Override
    public void carName() {
        System.out.println("Honda HRV");
    }
}
