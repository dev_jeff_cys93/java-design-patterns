package org.jc.solutions.design.patterns.structural.facade;

public class FacadeMain {
    public static void main(String[] args) {
        System.out.println("LEGACY WAY");
        printHondaCarsLegacy();

        System.out.println("\nFACADE WAY");
        printHondaCars();
    }

    static void printHondaCarsLegacy() {
        HondaCity hondaCity = new HondaCity();
        HondaJazz hondaJazz = new HondaJazz();
        HondaCivic hondaCivic = new HondaCivic();
        HondaBRV hondaBRV = new HondaBRV();
        HondaHRV hondaHRV = new HondaHRV();
        HondaCRV hondaCRV = new HondaCRV();

        hondaCity.carName();
        hondaJazz.carName();
        hondaCivic.carName();
        hondaBRV.carName();
        hondaHRV.carName();
        hondaCRV.carName();
    }

    static void printHondaCars() {
        HondaCarPrinter hondaCarPrinter = new HondaCarPrinter();
        hondaCarPrinter.printCity();
        hondaCarPrinter.printJazz();
        hondaCarPrinter.printCivic();
        hondaCarPrinter.printBRV();
        hondaCarPrinter.printHRV();
        hondaCarPrinter.printCRV();
    }
}
