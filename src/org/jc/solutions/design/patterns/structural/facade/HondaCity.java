package org.jc.solutions.design.patterns.structural.facade;

class HondaCity implements Car {
    @Override
    public void carName() {
        System.out.println("Honda City");
    }
}
