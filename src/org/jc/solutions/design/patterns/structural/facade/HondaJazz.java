package org.jc.solutions.design.patterns.structural.facade;

class HondaJazz implements Car {
    @Override
    public void carName() {
        System.out.println("Honda Jazz");
    }
}
