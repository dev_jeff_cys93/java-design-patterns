package org.jc.solutions.design.patterns.structural.facade;

class HondaCivic implements Car {
    @Override
    public void carName() {
        System.out.println("Honda Civic");
    }
}
