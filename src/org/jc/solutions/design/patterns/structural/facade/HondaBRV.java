package org.jc.solutions.design.patterns.structural.facade;

class HondaBRV implements Car {
    @Override
    public void carName() {
        System.out.println("Honda BRV");
    }
}
