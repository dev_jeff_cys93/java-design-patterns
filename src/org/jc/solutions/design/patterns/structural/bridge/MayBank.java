package org.jc.solutions.design.patterns.structural.bridge;

class MayBank implements Bank {
	private final static String BANK_NAME = "MayBank";
	private final static double INTEREST_RATE = 2.33;
	
	@Override
	public double getInterestRate() {
		return INTEREST_RATE;
	}

	@Override
	public String getBankName() {
		return BANK_NAME;
	}
}
