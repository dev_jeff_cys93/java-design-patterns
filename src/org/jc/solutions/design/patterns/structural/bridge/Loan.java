package org.jc.solutions.design.patterns.structural.bridge;

abstract class Loan {
	
	public double totalInterest(Bank bank) {
		return (bank.getInterestRate() / 100) * getLoanAmount() * getLoanTerm();
	}
	
	public double monthlyInstalment(Bank bank) {
		return (getLoanAmount() + totalInterest(bank)) / (getLoanTerm() * 12);
	}
	
	public void printStatement(Bank bank) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Bank Name:\t" + bank.getBankName());
		sb.append("\nInterest Rate:\t" + bank.getInterestRate());
		sb.append("\nLoan Amount:\t" + getLoanAmount());
		sb.append("\nLoan Term:\t" + getLoanTerm() + " years");
		sb.append("\nMonth Instalment:\t" + monthlyInstalment(bank));
		sb.append("\nTotal Interest:\t" + totalInterest(bank) + "\n");
		
		System.out.println(sb.toString());
	}
	
	abstract protected double getLoanAmount();
	
	abstract protected int getLoanTerm();
}
