package org.jc.solutions.design.patterns.structural.bridge;

public class BridgeMain {
	public static void main(String[] args) {
		Car civic = new Car("Civic TC", "Honda", 128000);
		Bank publicBank = new PublicBank();
		Bank mayBank = new MayBank();
		CarLoan carLoan = new CarLoan(civic, 9, 15000);
		
		carLoan.printStatement(publicBank); 
		carLoan.printStatement(mayBank);
	}
}
