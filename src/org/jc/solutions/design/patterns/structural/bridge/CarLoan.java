package org.jc.solutions.design.patterns.structural.bridge;

class CarLoan extends Loan {
	private Car car;
	private int loanTerm; // In years
	private double downPayment;
	
	public CarLoan(Car car, int loanTerm, double downPayment) {
		this.car = car;
		this.loanTerm = loanTerm;
		this.downPayment = downPayment;
	}
	
	@Override
	public void printStatement(Bank bank) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Car Model:\t" + car.getManufacturer() + " " + car.getCarModel());
		sb.append("\nDown Payment:\t" + downPayment);

		System.out.println(sb.toString());
		
		super.printStatement(bank);
	}
	
	@Override
	protected double getLoanAmount() {
		return car.getPrice() - downPayment;
	}
	@Override
	protected int getLoanTerm() {
		return loanTerm;
	}
}
