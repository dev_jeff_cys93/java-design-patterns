package org.jc.solutions.design.patterns.structural.bridge;

interface Bank {
	String getBankName();
	double getInterestRate();
}
