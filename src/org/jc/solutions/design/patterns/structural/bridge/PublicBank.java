package org.jc.solutions.design.patterns.structural.bridge;

public class PublicBank implements Bank {
	private final static String BANK_NAME = "Public Bank";
	private final static double INTEREST_RATE = 2.4	;
	
	@Override
	public double getInterestRate() {
		return INTEREST_RATE;
	}

	@Override
	public String getBankName() {
		return BANK_NAME;
	}
}
