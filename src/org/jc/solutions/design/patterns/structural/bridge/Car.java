package org.jc.solutions.design.patterns.structural.bridge;

class Car {
	private String carModel;
	private String manufacturer;
	private double price;
	
	public Car(String carModel, String manufacturer, double price) {
		this.carModel = carModel;
		this.manufacturer = manufacturer;
		this.price = price;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
