package org.jc.solutions.design.patterns.structural.proxy;

public class ProxyMain {
    public static void main(String[] args) {
        InvestmentPlatformService investmentPlatform = (InvestmentPlatformService) InvestmentPlatformProxy.newInstance(new FundInvestmentPlatform());
//        InvestmentPlatformService investmentPlatform = (InvestmentPlatformService) InvestmentPlatformProxy.newInstance(new StockInvestmentPlatform());

        investmentPlatform.buyStock();
        investmentPlatform.sellStock();

        investmentPlatform.buyFund();
        investmentPlatform.sellFund();
    }
}
