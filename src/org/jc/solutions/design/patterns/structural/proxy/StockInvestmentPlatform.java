package org.jc.solutions.design.patterns.structural.proxy;

class StockInvestmentPlatform implements InvestmentPlatformService {
    @Override
    public void buyFund() {
        System.out.println("restricted: buy fund");
    }

    @Override
    public void sellFund() {
        System.out.println("restricted: sell fund");
    }

    @Override
    public void buyStock() {
        System.out.println("buy stock");
    }

    @Override
    public void sellStock() {
        System.out.println("sell stock");
    }
}
