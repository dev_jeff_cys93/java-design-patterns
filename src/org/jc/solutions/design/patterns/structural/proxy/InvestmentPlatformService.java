package org.jc.solutions.design.patterns.structural.proxy;

interface InvestmentPlatformService {
    void buyFund();
    void sellFund();

    void buyStock();
    void sellStock();
}
