package org.jc.solutions.design.patterns.structural.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

class InvestmentPlatformProxy implements InvocationHandler {

    private Object obj;

    public InvestmentPlatformProxy(Object obj) {
        this.obj = obj;
    }

    public static Object newInstance(Object obj) {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new InvestmentPlatformProxy(obj));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        try {
            if (obj instanceof FundInvestmentPlatform) {
                System.out.println("FundInvestmentPlatform");
                if (!method.getName().toLowerCase().contains("fund")) {
                    throw new IllegalAccessException("Restricted to access non-Fund related methods.");
                }
            } else if (obj instanceof StockInvestmentPlatform) {
                System.out.println("StockInvestmentPlatform");
                if (!method.getName().toLowerCase().contains("stock")) {
                    throw new IllegalAccessException("Restricted to access non-Stock related methods.");
                }
            }

            result = method.invoke(obj, args);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected invocation exception: " + e.getMessage());
        }
        return result;
    }
}

