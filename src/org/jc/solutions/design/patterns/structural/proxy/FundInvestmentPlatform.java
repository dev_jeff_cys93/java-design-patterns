package org.jc.solutions.design.patterns.structural.proxy;

class FundInvestmentPlatform implements InvestmentPlatformService {
    @Override
    public void buyFund() {
        System.out.println("buy fund");
    }

    @Override
    public void sellFund() {
        System.out.println("sell fund");
    }

    @Override
    public void buyStock() {
        System.out.println("restricted: buy stock");
    }

    @Override
    public void sellStock() {
        System.out.println("restricted: sell stock");
    }
}
