package org.jc.solutions.design.patterns;

public class DesignPatternMain {
    public static void main(String[] args) {
        DesignPatternType.VISITOR.main(args);
    }
}
