package org.jc.solutions.design.patterns.creational.singleton;

public class SingletonMain {

	public static void main(String[] args) {
		Singleton instanceA = Singleton.getInstance();
		Singleton instanceB = Singleton.getInstance();

		System.out.println(instanceA);
		System.out.println(instanceB);

		if (instanceA == instanceB) {
			System.out.println("Same instance!");
		}
	}
}
