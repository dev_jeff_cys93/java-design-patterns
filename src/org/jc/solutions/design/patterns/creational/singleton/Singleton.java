package org.jc.solutions.design.patterns.creational.singleton;

class Singleton {
	// Lazy Loading - initialize to null, speed up startup time.
	 
	private static volatile Singleton instance = null;

	private Singleton() {
		// Protect instance get created from reflection method.
		if (instance != null) {
			throw new RuntimeException("Use getInstance() method to create.");
		}
	}

	public static Singleton getInstance() {
		// Lazy Loading - create new instance when needed during runtime.
		if (instance == null) {
			// Prevent multiple threads creating multiple instances at the same time.***
			synchronized (Singleton.class) {
				if (instance == null) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
}
