package org.jc.solutions.design.patterns.creational.builder;

public class BuilderMain {

	public static void main(String[] args) {
		DummyBean.Builder builder = new DummyBean.Builder();
		builder.dummy1("dummy1").dummy2("dummy2").dummy3("dummy3").dummy4("dummy4");

		DummyBean dummy = builder.build();
		System.out.println(dummy);
	}

}
