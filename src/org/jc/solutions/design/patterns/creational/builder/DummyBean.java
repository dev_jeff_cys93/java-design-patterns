package org.jc.solutions.design.patterns.creational.builder;

class DummyBean {

	public static class Builder {
		private String dummy1;
		private String dummy2;
		private String dummy3;
		private String dummy4;

		public DummyBean build() {
			return new DummyBean(this);
		}

		public Builder() {
		}

		public Builder dummy1(String dummy1) {
			this.dummy1 = dummy1;
			return this;
		}

		public Builder dummy2(String dummy2) {
			this.dummy2 = dummy2;
			return this;
		}

		public Builder dummy3(String dummy3) {
			this.dummy3 = dummy3;
			return this;
		}

		public Builder dummy4(String dummy4) {
			this.dummy4 = dummy4;
			return this;
		}
	}

	private final String dummy1;
	private final String dummy2;
	private final String dummy3;
	private final String dummy4;

	private DummyBean(Builder builder) {
		this.dummy1 = builder.dummy1;
		this.dummy2 = builder.dummy2;
		this.dummy3 = builder.dummy3;
		this.dummy4 = builder.dummy4;
	}

	public String getDummy1() {
		return dummy1;
	}

	public String getDummy2() {
		return dummy2;
	}

	public String getDummy3() {
		return dummy3;
	}

	public String getDummy4() {
		return dummy4;
	}

	@Override
	public String toString() {
		return "DummyBean [dummy1=" + dummy1 + ", dummy2=" + dummy2 + ", dummy3=" + dummy3 + ", dummy4=" + dummy4 + "]";
	}
}
