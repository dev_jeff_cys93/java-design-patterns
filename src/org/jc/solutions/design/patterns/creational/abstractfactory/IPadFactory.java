package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPadFactory extends AppleProductFactory  {

	@Override
	public IPad getAppleProduct(int budget) {
		if (budget < 0) {
			return null;
		} else if (budget < 2000) {
			return new IPadMini();
		} else if (budget < 3000) {
			return new IPadAir();
		} else {
			return new IPadPro();
		}
	}

}
