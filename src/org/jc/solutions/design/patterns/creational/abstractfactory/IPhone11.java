package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPhone11 extends IPhone {
	public IPhone11() {
		setProductName("iPhone 11");
	}
}
