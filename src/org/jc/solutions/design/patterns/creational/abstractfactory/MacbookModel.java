package org.jc.solutions.design.patterns.creational.abstractfactory;

public enum MacbookModel {
	MACBOOK_AIR,
	MACBOOK_PRO
}
