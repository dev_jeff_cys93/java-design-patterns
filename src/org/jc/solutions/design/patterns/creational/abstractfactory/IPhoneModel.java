package org.jc.solutions.design.patterns.creational.abstractfactory;

public enum IPhoneModel {
	IPHONE_11,
	IPHONE_11_PRO,
	IPHONE_11_PRO_MAX
}
