package org.jc.solutions.design.patterns.creational.abstractfactory;

abstract class AppleProductFactory {


	public static AppleProductFactory getAppleProductFactory(AppleProductType productType) {
		switch (productType) {
		case IPHONE: {
			return new IPhoneFactory();
		}
		case IPAD: {
			return new IPadFactory();
		}
		case MACBOOK: {
			return new MacbookFactory();
		}
		}
		return null;
	}
	
	public abstract AppleProduct getAppleProduct(int budget);
}
