package org.jc.solutions.design.patterns.creational.abstractfactory;

class MacbookFactory extends AppleProductFactory  {

	@Override
	public Macbook getAppleProduct(int budget) {
		if (budget < 0) {
			return null;
		} else if (budget < 5000) {
			return new MacbookAir();
		} else {
			return  new MacbookPro();
		}
	}

}
