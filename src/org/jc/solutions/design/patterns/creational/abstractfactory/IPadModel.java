package org.jc.solutions.design.patterns.creational.abstractfactory;

enum IPadModel {
	IPAD_MINI,
	IPAD_AIR,
	IPAD_PRO
}
