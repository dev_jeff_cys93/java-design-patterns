package org.jc.solutions.design.patterns.creational.abstractfactory;

abstract class AppleProduct {
	private String productName;

	public String getProductName() {
		return productName;
	}

	protected void setProductName(String productName) {
		this.productName = productName;
	}
}
