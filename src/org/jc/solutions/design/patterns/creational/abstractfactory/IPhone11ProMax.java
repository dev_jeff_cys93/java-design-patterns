package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPhone11ProMax extends IPhone {
	public IPhone11ProMax() {
		setProductName("iPhone 11 Pro Max");
	}
}
