package org.jc.solutions.design.patterns.creational.abstractfactory;

class MacbookAir extends Macbook {
	public MacbookAir() {
		setProductName("Macbook Air");
	}
}
