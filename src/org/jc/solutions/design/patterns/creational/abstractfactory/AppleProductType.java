package org.jc.solutions.design.patterns.creational.abstractfactory;

enum AppleProductType {
	IPAD,
	IPHONE,
	MACBOOK
}
