package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPhone11Pro extends IPhone {
	public IPhone11Pro() {
		setProductName("iPhone 11 Pro");
	}
}
