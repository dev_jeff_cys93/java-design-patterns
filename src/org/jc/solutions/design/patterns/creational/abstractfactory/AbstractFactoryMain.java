package org.jc.solutions.design.patterns.creational.abstractfactory;

public class AbstractFactoryMain {

	public static void main(String[] args) {
		AppleProductFactory iPhoneFactory = AppleProductFactory.getAppleProductFactory(AppleProductType.IPHONE);
		AppleProduct iPhoneA = iPhoneFactory.getAppleProduct(3000);
		AppleProduct iPhoneB = iPhoneFactory.getAppleProduct(5000);
		AppleProduct iPhoneC = iPhoneFactory.getAppleProduct(2000);
		printAppleProduct(iPhoneA);
		printAppleProduct(iPhoneB);
		printAppleProduct(iPhoneC);

		AppleProductFactory iPadFactory = AppleProductFactory.getAppleProductFactory(AppleProductType.IPAD);
		AppleProduct iPadA = iPadFactory.getAppleProduct(3000);
		AppleProduct iPadB = iPadFactory.getAppleProduct(1000);
		AppleProduct iPadC = iPadFactory.getAppleProduct(2000);
		printAppleProduct(iPadA);
		printAppleProduct(iPadB);
		printAppleProduct(iPadC);

		AppleProductFactory macbookFactory = AppleProductFactory.getAppleProductFactory(AppleProductType.MACBOOK);
		AppleProduct macbookA = macbookFactory.getAppleProduct(3000);
		AppleProduct macbookB = macbookFactory.getAppleProduct(5000);
		AppleProduct macbookC = macbookFactory.getAppleProduct(7000);
		printAppleProduct(macbookA);
		printAppleProduct(macbookB);
		printAppleProduct(macbookC);
	}

	private static void printAppleProduct(AppleProduct appleProduct) {
		System.out.println(appleProduct.getClass().getName() + ": \t\t" + appleProduct.getProductName());
	}
}
