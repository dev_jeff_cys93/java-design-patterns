package org.jc.solutions.design.patterns.creational.abstractfactory;

class MacbookPro extends Macbook {
	public MacbookPro() {
		setProductName("Macbook Pro");
	}
}
