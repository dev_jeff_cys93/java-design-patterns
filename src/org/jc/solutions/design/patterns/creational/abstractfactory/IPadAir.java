package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPadAir extends IPad {
	public IPadAir() {
		setProductName("iPad Air");
	}
}
