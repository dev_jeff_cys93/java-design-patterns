package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPadMini extends IPad {
	public IPadMini() {
		setProductName("iPad Mini");
	}
}
