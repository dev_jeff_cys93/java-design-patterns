package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPadPro extends IPad {
	public IPadPro() {
		setProductName("iPad Pro");
	}
}
