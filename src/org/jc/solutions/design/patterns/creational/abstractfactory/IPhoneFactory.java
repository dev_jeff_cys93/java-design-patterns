package org.jc.solutions.design.patterns.creational.abstractfactory;

class IPhoneFactory extends AppleProductFactory {

	@Override
	public IPhone getAppleProduct(int budget) {
		if (budget < 0) {
			return null;
		} else if (budget < 3000) {
			return new IPhone11();
		} else if (budget < 4000) {
			return new IPhone11Pro();
		} else {
			return new IPhone11ProMax();
		}
	}

}
