package org.jc.solutions.design.patterns.creational.prototype;

class Movie extends Item {
	private int runTime;
	private ShallowCopy shallowCopy;

	public int getRunTime() {
		return runTime;
	}

	public void setRunTime(int runTime) {
		this.runTime = runTime;
	}

	public ShallowCopy getShallowCopy() {
		return shallowCopy;
	}

	public void setShallowCopy(ShallowCopy shallowCopy) {
		this.shallowCopy = shallowCopy;
	}

}
