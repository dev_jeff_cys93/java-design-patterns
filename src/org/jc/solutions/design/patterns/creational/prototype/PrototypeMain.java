package org.jc.solutions.design.patterns.creational.prototype;

public class PrototypeMain {

	public static void main(String[] args) {
		ItemRegistry registry = new ItemRegistry();

		// Deep Copy Example
		System.out.println("Deep Copy");
		Book book1 = (Book) registry.createItem(ItemRegistry.Type.BOOK);
		book1.setTitle("Book 1");
		printBook(book1);

		Book book2 = (Book) registry.createItem(ItemRegistry.Type.BOOK);
		book2.setTitle("Book 2");
		book2.setNoOfPages(10);
		printBook(book2);

		// Shallow Copy Example
		System.out.println("\nShallow Copy");
		Movie movie1 = (Movie) registry.createItem(ItemRegistry.Type.MOVIE);
		movie1.setTitle("Movie 1");
		movie1.setRunTime(1000);
		printMovie(movie1);

		Movie movie2 = (Movie) registry.createItem(ItemRegistry.Type.MOVIE);
		movie2.setTitle("Movie 2");
		movie2.setRunTime(10);
		printMovie(movie2);
	}

	private static void printBook(Book book) {
		System.out.println(book.getTitle());
		System.out.println(book);
//		System.out.println(book.getPrice());
//		System.out.println(book.getUrl());
		System.out.println(book.getNoOfPages());
		System.out.println(book.getDeepCopy()); // Print memory address
	}

	private static void printMovie(Movie movie) {
		System.out.println(movie.getTitle());
		System.out.println(movie);
//		System.out.println(movie.getPrice());
//		System.out.println(movie.getUrl());
		System.out.println(movie.getRunTime());
		System.out.println(movie.getShallowCopy()); // Print memory address
	}
}
