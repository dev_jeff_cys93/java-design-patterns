package org.jc.solutions.design.patterns.creational.prototype;

class Book extends Item {
	private int noOfPages;
	private DeepCopy deepCopy;

	// Deep Copy
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Book book = (Book) super.clone();
		book.deepCopy = (DeepCopy) deepCopy.clone();

		return book;
	}

	public int getNoOfPages() {
		return noOfPages;
	}

	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	public DeepCopy getDeepCopy() {
		return deepCopy;
	}

	public void setDeepCopy(DeepCopy deepCopy) {
		this.deepCopy = deepCopy;
	}
}
