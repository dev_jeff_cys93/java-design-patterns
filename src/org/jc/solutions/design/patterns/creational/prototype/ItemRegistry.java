package org.jc.solutions.design.patterns.creational.prototype;

import java.util.HashMap;
import java.util.Map;

class ItemRegistry {
	enum Type {
		MOVIE, BOOK
	}

	private Map<Type, Item> items = new HashMap<>();

	ItemRegistry() {
		loadItems();
	}

	public Item createItem(Type type) {
		Item item = null;

		try {
			item = (Item) items.get(type).clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Exception Handler.");
			e.printStackTrace();
		}

		return item;
	}

	private void loadItems() {
		Movie movie = new Movie();
		movie.setTitle("Movie Title");
		movie.setUrl("Movie URL");
		movie.setPrice(20);
		movie.setRunTime(120);
		movie.setShallowCopy(new ShallowCopy());
		items.put(Type.MOVIE, movie);

		Book book = new Book();
		book.setTitle("Book Title");
		book.setUrl("Book URL");
		book.setPrice(50);
		book.setNoOfPages(300);
		book.setDeepCopy(new DeepCopy());
		items.put(Type.BOOK, book);
	}
}
