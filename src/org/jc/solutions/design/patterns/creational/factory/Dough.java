package org.jc.solutions.design.patterns.creational.factory;

class Dough extends Ingredient {
	public String toString() {
		return "Dough";
	}
}
