package org.jc.solutions.design.patterns.creational.factory;

import java.util.ArrayList;

class ChickenHawaiianPizza extends Pizza {
	public ChickenHawaiianPizza() {
		ingredients = new ArrayList<>();
		ingredients.add(new Dough());
		ingredients.add(new TomatoSauce());
		ingredients.add(new Cheese());
		ingredients.add(new Chicken());
	}
	
	public String toString() {
		return "ChickenHawaiianPizza";
	}
}
