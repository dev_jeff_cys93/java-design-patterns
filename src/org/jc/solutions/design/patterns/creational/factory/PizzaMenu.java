package org.jc.solutions.design.patterns.creational.factory;

enum PizzaMenu {
	CHEESE_PIZZA,
	PEPERONI_PIZZA,
	CHICKEN_HAWAIIAN_PIZZA
}
