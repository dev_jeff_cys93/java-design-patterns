package org.jc.solutions.design.patterns.creational.factory;

class Peperoni extends Ingredient {
	public String toString() {
		return "Peperoni";
	}
}
