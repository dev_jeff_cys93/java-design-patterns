package org.jc.solutions.design.patterns.creational.factory;

class Cheese extends Ingredient {
	public String toString() {
		return "Cheese";
	}
}
