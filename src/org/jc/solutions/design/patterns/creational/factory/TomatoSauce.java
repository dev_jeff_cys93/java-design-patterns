package org.jc.solutions.design.patterns.creational.factory;

class TomatoSauce extends Ingredient {
	public String toString() {
		return "TomatoSauce";
	}
}
