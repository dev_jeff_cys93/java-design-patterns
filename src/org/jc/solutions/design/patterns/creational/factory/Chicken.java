package org.jc.solutions.design.patterns.creational.factory;

class Chicken extends Ingredient {
	public String toString() {
		return "Chicken";
	}
}
