package org.jc.solutions.design.patterns.creational.factory;

import java.util.ArrayList;

class CheesePizza extends Pizza {
	public CheesePizza() {
		ingredients = new ArrayList<>();
		ingredients.add(new Dough());
		ingredients.add(new TomatoSauce());
		ingredients.add(new Cheese());
	}
	
	public String toString() {
		return "CheesePizza";
	}
}
