package org.jc.solutions.design.patterns.creational.factory;

import java.util.List;

class Pizza {
	protected List<Ingredient> ingredients;

	public List<Ingredient> getIngredients() {
		return ingredients;
	}
}
