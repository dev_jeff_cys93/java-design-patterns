package org.jc.solutions.design.patterns.creational.factory;

import java.sql.SQLException;

public class FactoryMain {

	public static void main(String[] args) throws SQLException {
		Pizza cheezePizza = PizzaFactory.getPizza(PizzaMenu.CHEESE_PIZZA);
		System.out.println(cheezePizza);
		System.out.println(cheezePizza.getIngredients());
		
		Pizza peperoniPizza = PizzaFactory.getPizza(PizzaMenu.PEPERONI_PIZZA);
		System.out.println(peperoniPizza);
		System.out.println(peperoniPizza.getIngredients());

		Pizza chickenHawaiianPizza = PizzaFactory.getPizza(PizzaMenu.CHICKEN_HAWAIIAN_PIZZA);
		System.out.println(chickenHawaiianPizza);
		System.out.println(chickenHawaiianPizza.getIngredients());
	}
}