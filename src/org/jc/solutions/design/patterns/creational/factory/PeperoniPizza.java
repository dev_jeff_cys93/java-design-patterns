package org.jc.solutions.design.patterns.creational.factory;

import java.util.ArrayList;

class PeperoniPizza extends Pizza {
	public PeperoniPizza() {
		ingredients = new ArrayList<>();
		ingredients.add(new Dough());
		ingredients.add(new TomatoSauce());
		ingredients.add(new Cheese());
		ingredients.add(new Peperoni());
	}
	
	public String toString() {
		return "PeperoniPizza";
	}
}
