package org.jc.solutions.design.patterns.creational.factory;

class Pineapple extends Ingredient {
	public String toString() {
		return "Pineapple";
	}
}
