package org.jc.solutions.design.patterns.creational.factory;

class PizzaFactory {
	static Pizza getPizza(PizzaMenu pizzaMenu) {
		switch (pizzaMenu) {
		case CHEESE_PIZZA:
			return new CheesePizza();

		case PEPERONI_PIZZA:
			return new PeperoniPizza();

		case CHICKEN_HAWAIIAN_PIZZA:
			return new ChickenHawaiianPizza();

		default:
			return null;
		}
	}
}
