package org.jc.solutions.design.patterns;

import org.jc.solutions.design.patterns.behavioral.chainofresponsibility.ChainOfResponsibilityMain;
import org.jc.solutions.design.patterns.behavioral.command.CommandMain;
import org.jc.solutions.design.patterns.behavioral.interpreter.InterpreterMain;
import org.jc.solutions.design.patterns.behavioral.iterator.IteratorMain;
import org.jc.solutions.design.patterns.behavioral.mediator.MediatorMain;
import org.jc.solutions.design.patterns.behavioral.memento.MementoMain;
import org.jc.solutions.design.patterns.behavioral.observer.ObserverMain;
import org.jc.solutions.design.patterns.behavioral.state.StateMain;
import org.jc.solutions.design.patterns.behavioral.strategy.StrategyMain;
import org.jc.solutions.design.patterns.behavioral.templatemethod.TemplateMethodMain;
import org.jc.solutions.design.patterns.behavioral.visitor.VisitorMain;
import org.jc.solutions.design.patterns.creational.abstractfactory.AbstractFactoryMain;
import org.jc.solutions.design.patterns.creational.builder.BuilderMain;
import org.jc.solutions.design.patterns.creational.prototype.PrototypeMain;
import org.jc.solutions.design.patterns.creational.singleton.SingletonMain;
import org.jc.solutions.design.patterns.structural.adapter.AdapterMain;
import org.jc.solutions.design.patterns.structural.bridge.BridgeMain;
import org.jc.solutions.design.patterns.structural.composite.CompositeMain;
import org.jc.solutions.design.patterns.structural.decorator.DecoratorMain;
import org.jc.solutions.design.patterns.structural.facade.FacadeMain;
import org.jc.solutions.design.patterns.structural.flyweight.FlyweightMain;
import org.jc.solutions.design.patterns.structural.proxy.ProxyMain;

import java.util.function.Consumer;

public enum DesignPatternType {
    // CREATIONAL
    SINGLETON(SingletonMain::main),
    BUILDER(BuilderMain::main),
    PROTOTYPE(PrototypeMain::main),
    FACTORY(FacadeMain::main),
    ABSTRACT_FACTORY(AbstractFactoryMain::main),

    // STRUCTURAL
    ADAPTER(AdapterMain::main),
    BRIDGE(BridgeMain::main),
    COMPOSITE(CompositeMain::main),
    DECORATOR(DecoratorMain::main),
    FACADE(FacadeMain::main),
    FLYWEIGHT(FlyweightMain::main),
    PROXY(ProxyMain::main),

    // BEHAVIORAL
    CHAIN_OF_RESPONSIBILITY(ChainOfResponsibilityMain::main),
    COMMAND(CommandMain::main),
    INTERPRETER(InterpreterMain::main),
    ITERATOR(IteratorMain::main),
    MEDIATOR(MediatorMain::main),
    MEMENTO(MementoMain::main),
    OBSERVER(ObserverMain::main),
    STATE(StateMain::main),
    STRATEGY(StrategyMain::main),
    TEMPLATE_METHOD(TemplateMethodMain::main),
    VISITOR(VisitorMain::main);

    private Consumer<String[]> main;

    DesignPatternType(Consumer<String[]> main) {
        this.main = main;
    }

    public void main(String[] args) {
        main.accept(args);
    }
}
