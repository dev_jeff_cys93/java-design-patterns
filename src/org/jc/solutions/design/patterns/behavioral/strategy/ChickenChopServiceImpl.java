package org.jc.solutions.design.patterns.behavioral.strategy;

public class ChickenChopServiceImpl implements FoodService {
    @Override
    public void serve() {
        System.out.println("Chicken Chop.");
    }
}
