package org.jc.solutions.design.patterns.behavioral.strategy;

enum FoodPreferenceType {
    CHINESE, INDIAN, WESTERN, NO_PREFERENCE
}
