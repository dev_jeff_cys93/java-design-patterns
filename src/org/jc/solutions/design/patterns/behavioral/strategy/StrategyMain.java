package org.jc.solutions.design.patterns.behavioral.strategy;

public class StrategyMain {
    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        Person personA = new Person("Person A", FoodPreferenceType.CHINESE);
        Person personB = new Person("Person B", FoodPreferenceType.INDIAN);
        Person personC = new Person("Person C");

        restaurant.servePreferenceFood(personA);
        restaurant.servePreferenceFood(personB);
        restaurant.servePreferenceFood(personC);
    }
}
