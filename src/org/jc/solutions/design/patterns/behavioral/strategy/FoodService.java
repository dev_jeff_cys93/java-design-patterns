package org.jc.solutions.design.patterns.behavioral.strategy;

interface FoodService {
    void serve();
}
