package org.jc.solutions.design.patterns.behavioral.strategy;

class RotiCanaiServiceImpl implements FoodService {
    @Override
    public void serve() {
        System.out.println("Roti Canai.");
    }
}
