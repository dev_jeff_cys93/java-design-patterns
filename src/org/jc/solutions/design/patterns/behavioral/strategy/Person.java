package org.jc.solutions.design.patterns.behavioral.strategy;

class Person {
    private final String name;
    private final FoodPreferenceType foodPreferenceType;

    public Person(String name) {
        this.name = name;
        this.foodPreferenceType = FoodPreferenceType.NO_PREFERENCE;
    }

    public Person(String name, FoodPreferenceType foodPreferenceType) {
        this.name = name;
        this.foodPreferenceType = foodPreferenceType;
    }

    public String getName() {
        return name;
    }

    public FoodPreferenceType getFoodPreferenceType() {
        return foodPreferenceType;
    }
}
