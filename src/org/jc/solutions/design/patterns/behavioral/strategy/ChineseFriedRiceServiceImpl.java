package org.jc.solutions.design.patterns.behavioral.strategy;

class ChineseFriedRiceServiceImpl implements FoodService {
    @Override
    public void serve() {
        System.out.println("Chinese Fried Rice.");
    }
}
