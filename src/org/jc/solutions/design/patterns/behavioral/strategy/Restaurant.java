package org.jc.solutions.design.patterns.behavioral.strategy;

import java.util.Random;

class Restaurant {
    public void servePreferenceFood(Person person) {
        FoodPreferenceType foodPreferenceType = person.getFoodPreferenceType();
        if (foodPreferenceType == FoodPreferenceType.NO_PREFERENCE) {
            FoodPreferenceType[] types = FoodPreferenceType.values();
            Random random = new Random();
            int index = random.nextInt(types.length - 1);
            foodPreferenceType = types[index];
        }
        getFoodService(foodPreferenceType).serve();
    }

    private FoodService getFoodService(FoodPreferenceType foodPreferenceType) {
        switch(foodPreferenceType) {
            case CHINESE:
                return new ChineseFriedRiceServiceImpl();
            case INDIAN:
                return new RotiCanaiServiceImpl();
            case WESTERN:
                return new ChickenChopServiceImpl();
            default:
                throw new IllegalArgumentException("Invalid food preference.");
        }
    }
}
