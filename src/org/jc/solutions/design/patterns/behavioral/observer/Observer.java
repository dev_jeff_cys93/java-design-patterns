package org.jc.solutions.design.patterns.behavioral.observer;

abstract class Observer {
    protected Subject subject;
    abstract void update();
}
