package org.jc.solutions.design.patterns.behavioral.observer;

class PhoneClient extends Observer {
    public PhoneClient(Subject subject) {
        this.subject = subject;
        subject.attach(this);
    }

    public void addMessage(String message) {
        subject.setState(message + " - Sent from phone.");
    }

    @Override
    void update() {
        System.out.println("Phone stream: " + subject.getState());
    }
}
