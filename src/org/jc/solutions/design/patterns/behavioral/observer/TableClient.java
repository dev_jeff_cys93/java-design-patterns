package org.jc.solutions.design.patterns.behavioral.observer;

class TableClient extends Observer {
    public TableClient(Subject subject) {
        this.subject = subject;
        subject.attach(this);
    }

    public void addMessage(String message) {
        subject.setState(message + " - Sent from tablet.");
    }

    @Override
    void update() {
        System.out.println("Tablet stream: " + subject.getState());
    }
}
