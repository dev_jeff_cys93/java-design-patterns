package org.jc.solutions.design.patterns.behavioral.observer;

import java.util.ArrayDeque;
import java.util.Deque;

class MessageStream extends Subject {
    private Deque<String> messageHistory = new ArrayDeque<>();

    @Override
    void setState(String state) {
        messageHistory.add(state);
        this.notifyObservers();
    }

    @Override
    String getState() {
        return messageHistory.getLast();
    }
}
