package org.jc.solutions.design.patterns.behavioral.observer;

public class ObserverMain {
    public static void main(String[] args) {
        MessageStream stream = new MessageStream();
        PhoneClient phoneClient = new PhoneClient(stream);
        TableClient tableClient = new TableClient(stream);

        phoneClient.addMessage("Rainny day huh?");
        tableClient.addMessage("It's sunny here.");
    }
}
