package org.jc.solutions.design.patterns.behavioral.command;

import java.util.List;

abstract class CarModeCommandBase implements Command {
    protected List<CarComponent> carComponents;

    public void setCarComponents(List<CarComponent> carComponents) {
        this.carComponents = carComponents;
    }

    @Override
    public void execute() {
        System.out.println(getMode());
        for (CarComponent carComponent: carComponents) {
            if (carComponent instanceof  Aircond) {
                setAircondMode(carComponent);
            } else if (carComponent instanceof  Gearbox) {
                setGearboxMode(carComponent);
            } else if (carComponent instanceof  Steering) {
                setSteeringMode(carComponent);
            } else if (carComponent instanceof  Suspension) {
                setSuspensionMode(carComponent);
            } else if (carComponent instanceof  Throttle) {
                setThrottleMode(carComponent);
            }
            System.out.println(carComponent.toString());
        }
    }

    abstract String getMode();
    abstract void setAircondMode(CarComponent carComponent);
    abstract void setGearboxMode(CarComponent carComponent);
    abstract void setSteeringMode(CarComponent carComponent);
    abstract void setSuspensionMode(CarComponent carComponent);
    abstract void setThrottleMode(CarComponent carComponent);
}
