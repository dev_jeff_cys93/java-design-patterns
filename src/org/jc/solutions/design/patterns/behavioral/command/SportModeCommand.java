package org.jc.solutions.design.patterns.behavioral.command;

import java.util.List;

/**
 * Concrete {@code Command}
 */
class SportModeCommand extends CarModeCommandBase {
    public SportModeCommand(List<CarComponent> carComponents) {
        setCarComponents(carComponents);
    }

    @Override
    String getMode() {
        return "Sport Mode";
    }

    @Override
    void setAircondMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setGearboxMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.AGGRESSIVE);
    }

    @Override
    void setSteeringMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.HARD);
    }

    @Override
    void setSuspensionMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.HARD);
    }

    @Override
    void setThrottleMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.AGGRESSIVE);
    }
}
