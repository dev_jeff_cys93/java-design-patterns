package org.jc.solutions.design.patterns.behavioral.command;

interface Command {
    void execute();
}
