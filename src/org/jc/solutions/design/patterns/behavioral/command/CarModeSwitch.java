package org.jc.solutions.design.patterns.behavioral.command;

/**
 * Invoker
 */
class CarModeSwitch {
    public void storeAndExecute(Command command) {
        command.execute();
    }
}
