package org.jc.solutions.design.patterns.behavioral.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandMain {
    public static void main(String[] args) {
        List<CarComponent> carComponents = new ArrayList<>(
            Arrays.asList(
                new Aircond(),
                new Gearbox(),
                new Steering(),
                new Suspension(),
                new Throttle()
            )
        );

        Command normalMode = new NormalModeCommand(carComponents);
        Command sportMode = new SportModeCommand(carComponents);
        Command ecoMode = new EcoModeCommand(carComponents);

        CarModeSwitch carModeSwitch = new CarModeSwitch();
        carModeSwitch.storeAndExecute(normalMode);
        System.out.println("");
        carModeSwitch.storeAndExecute(sportMode);
        System.out.println("");
        carModeSwitch.storeAndExecute(ecoMode);
    }
}
