package org.jc.solutions.design.patterns.behavioral.command;

import java.util.List;

/**
 * Concrete {@code Command}
 */
class NormalModeCommand extends CarModeCommandBase {
    public NormalModeCommand(List<CarComponent> carComponents) {
        setCarComponents(carComponents);
    }

    @Override
    String getMode() {
        return "Normal Mode";
    }

    @Override
    void setAircondMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setGearboxMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setSteeringMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setSuspensionMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setThrottleMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }
}
