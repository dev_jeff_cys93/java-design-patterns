package org.jc.solutions.design.patterns.behavioral.command;

import java.util.List;

/**
 * Concrete {@code Command}
 */
class EcoModeCommand extends CarModeCommandBase {
    public EcoModeCommand(List<CarComponent> carComponents) {
        setCarComponents(carComponents);
    }

    @Override
    String getMode() {
        return "Eco Mode";
    }

    @Override
    void setAircondMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.CONSERVATIVE);
    }

    @Override
    void setGearboxMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.CONSERVATIVE);
    }

    @Override
    void setSteeringMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setSuspensionMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.NORMAL);
    }

    @Override
    void setThrottleMode(CarComponent carComponent) {
        carComponent.setMode(CarComponent.CONSERVATIVE);
    }
}
