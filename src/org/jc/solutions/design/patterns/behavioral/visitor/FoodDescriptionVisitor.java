package org.jc.solutions.design.patterns.behavioral.visitor;

class FoodDescriptionVisitor implements FoodVisitor {
    @Override
    public void visit(NasiGorengKampung food) {
        System.out.println("This is Kampung Fried Rice.");
    }

    @Override
    public void visit(RamlyBurger food) {
        System.out.println("This is Ramly Burger.");
    }

    @Override
    public void visit(NasiLemak food) {
        System.out.println("This is Nasi Lemak.");
    }

    @Override
    public void visit(FoodOrder order) {

    }
}
