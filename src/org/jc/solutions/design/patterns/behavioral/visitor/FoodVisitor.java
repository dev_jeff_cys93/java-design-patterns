package org.jc.solutions.design.patterns.behavioral.visitor;

interface FoodVisitor {
    void visit(NasiGorengKampung food);
    void visit(RamlyBurger food);
    void visit(NasiLemak food);
    void visit(FoodOrder order);
}
