package org.jc.solutions.design.patterns.behavioral.visitor;

public class VisitorMain {
    public static void main(String[] args) {
        FoodOrder order = new FoodOrder();
        order.addFood(new NasiLemak());
        order.addFood(new RamlyBurger());
        order.addFood(new NasiGorengKampung());

        order.accept(new FoodDeliveryVisitor());
        order.accept(new FoodDescriptionVisitor());
    }
}
