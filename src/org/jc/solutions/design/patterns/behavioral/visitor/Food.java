package org.jc.solutions.design.patterns.behavioral.visitor;

interface Food {
    void accept(FoodVisitor visitor);
}
