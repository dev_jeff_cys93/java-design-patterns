package org.jc.solutions.design.patterns.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

class FoodOrder implements Food {
    List<Food> foods = new ArrayList<>();

    public void addFood(Food food) {
        foods.add(food);
    }

    public List<Food> getFoods() {
        return foods;
    }

    @Override
    public void accept(FoodVisitor visitor) {
        for (Food food: foods) {
            food.accept(visitor);
        }
        visitor.visit(this);
    }
}
