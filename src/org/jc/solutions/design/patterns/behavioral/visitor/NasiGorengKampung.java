package org.jc.solutions.design.patterns.behavioral.visitor;

class NasiGorengKampung implements Food {
    @Override
    public void accept(FoodVisitor visitor) {
        visitor.visit(this);
    }
}
