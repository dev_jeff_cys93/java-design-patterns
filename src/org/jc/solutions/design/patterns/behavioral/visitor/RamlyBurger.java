package org.jc.solutions.design.patterns.behavioral.visitor;

class RamlyBurger implements Food {
    @Override
    public void accept(FoodVisitor visitor) {
        visitor.visit(this);
    }
}
