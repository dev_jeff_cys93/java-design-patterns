package org.jc.solutions.design.patterns.behavioral.visitor;

class FoodDeliveryVisitor implements FoodVisitor {
    double totalPrice = 0;
    double deliveryFee = 0;

    @Override
    public void visit(NasiGorengKampung food) {
        System.out.println("Nasi Goreng Kampung: 6");
        totalPrice += 6;
    }

    @Override
    public void visit(RamlyBurger food) {
        System.out.println("Ramly Burger: 4");
        totalPrice += 4;
    }

    @Override
    public void visit(NasiLemak food) {
        System.out.println("Nasi Lemak: 3");
        totalPrice += 3;
    }

    @Override
    public void visit(FoodOrder order) {
        deliveryFee = 5;
        if (totalPrice >= 20) {
            deliveryFee = 0;
        }

        System.out.println("Total Price: " + totalPrice);
        System.out.println("Delivery Fee: " + deliveryFee);
    }
}
