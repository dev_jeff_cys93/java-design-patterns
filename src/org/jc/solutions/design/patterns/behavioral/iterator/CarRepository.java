package org.jc.solutions.design.patterns.behavioral.iterator;

import java.util.Iterator;
import java.util.function.Consumer;

class CarRepository implements Iterable<String> {

    private String[] cars;
    private int index;

    public CarRepository() {
        cars = new String[10];
        index = 0;
    }

    public void addCar(String car) {
        if (index == cars.length) {
            String[] largerCars = new String[cars.length + 5];
            System.arraycopy(cars, 0 , largerCars, 0, cars.length);
            cars = largerCars;
            largerCars = null;
        }
        cars[index] = car;
        index++;
    }

    @Override
    public Iterator<String> iterator() {
        Iterator<String> it = new Iterator<String>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < cars.length && cars[currentIndex] != null;
            }

            @Override
            public String next() {
                return cars[currentIndex++];
            }

            @Override
            public void remove() {
                int numMoved = index - currentIndex + 1;
                if (numMoved > 0) {
                    System.arraycopy(cars, currentIndex, cars, --currentIndex, numMoved);
                }
            }
            private String printCars() {
                String text = "";
                int index = 0;
                for(String car: cars) {
                    text += car;
                    if (index != cars.length - 1) {
                        text += ",";
                    }
                    index++;
                }
                return text;
            }
        };
        return it;
    }

    @Override
    public void forEach(Consumer action) {
        for (String car: cars) {
            action.accept(car);
        }
    }
}
