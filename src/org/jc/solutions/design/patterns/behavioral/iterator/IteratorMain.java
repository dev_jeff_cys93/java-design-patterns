package org.jc.solutions.design.patterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IteratorMain {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepository();
        carRepository.addCar("Honda Civic");
        carRepository.addCar("Mazda 3");
        carRepository.addCar("Honda Civic 1.8");
        carRepository.addCar("Mazda 2");
        carRepository.addCar("Mazda 3");
        carRepository.addCar("Mazda 6");
        carRepository.addCar("Toyota Altis");

        List<String> test = new ArrayList<>();
        test.iterator();

        Iterator<String> it = carRepository.iterator();
        while(it.hasNext()) {
            String car  = it.next();
            if (car.equals("Mazda 3")) {
                it.remove();
            }
            else {
                System.out.println(car);
            }
        }

        carRepository.forEach(System.out::print);
    }
}
