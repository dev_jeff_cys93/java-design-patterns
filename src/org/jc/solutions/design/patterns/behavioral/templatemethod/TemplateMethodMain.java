package org.jc.solutions.design.patterns.behavioral.templatemethod;

public class TemplateMethodMain {
    public static void main(String[] args) {
        CakeTemplate cheeseCake = new CheeseCake();
        CakeTemplate chocolateCake = new ChocolateCake();

        cheeseCake.prepare();
        chocolateCake.prepare();
    }
}
