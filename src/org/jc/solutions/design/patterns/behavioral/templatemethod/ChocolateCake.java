package org.jc.solutions.design.patterns.behavioral.templatemethod;

class ChocolateCake extends CakeTemplate {
    public ChocolateCake() {
        this.needToBake = true;
    }

    @Override
    void mix() {
        System.out.println("Mixing cream cheese, condensed milk, egg, lemon juice and gelatin.");
    }

    @Override
    void bake() {
        System.out.println("Put into oven and bake for 30 minutes at 160 degree Celsius.");
    }

    @Override
    void noBake() throws IllegalAccessException {
        throw new IllegalAccessException("Baking is needed.");
    }

    @Override
    void decorate() {
        System.out.println("Pour some chocolate on top of the cake.");
    }
}
