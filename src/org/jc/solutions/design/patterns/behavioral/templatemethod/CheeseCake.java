package org.jc.solutions.design.patterns.behavioral.templatemethod;

class CheeseCake extends CakeTemplate {
    public CheeseCake() {
        this.needToBake = false;
    }

    @Override
    void mix() {
        System.out.println("Mixing cream cheese, condensed milk, egg, lemon juice and gelatin.");
    }

    @Override
    void bake() throws IllegalAccessException {
        throw new IllegalAccessException("Baking is not needed.");
    }

    @Override
    void noBake() {
        System.out.println("Put into fridge for 2 hours.");
    }

    @Override
    void decorate() {
        System.out.println("Pour some lemon zest on top of the cake.");
    }
}
