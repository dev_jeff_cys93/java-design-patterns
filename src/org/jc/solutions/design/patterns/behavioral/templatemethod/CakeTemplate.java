package org.jc.solutions.design.patterns.behavioral.templatemethod;

abstract class CakeTemplate {
    abstract void mix();
    abstract void bake() throws IllegalAccessException;
    abstract void noBake() throws IllegalAccessException;
    abstract void decorate();

    protected boolean needToBake = true;

    public void prepare() {
        try {
            mix();
            if (needToBake) {
                bake();
            } else {
                noBake();
            }
            decorate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
