package org.jc.solutions.design.patterns.behavioral.mediator;

public class MediatorMain {
    public static void main(String[] args) {
        mediatorOnly();
        mediatorWithCommand();
    }

    private static void mediatorOnly() {
        System.out.println("MEDIATOR ONLY");
        ChatMediator mediator = new ChatMediatorImpl();
        User user1 = new UserImpl(mediator, "Pankaj");
        User user2 = new UserImpl(mediator, "Lisa");
        User user3 = new UserImpl(mediator, "Saurabh");
        User user4 = new UserImpl(mediator, "David");
        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);

        user1.send("Hi All");
    }

    private static void mediatorWithCommand() {
        System.out.println("\nMEDIATOR WITH COMMAND");
        CarModeMediator carModeMediator = new CarModeCarModeMediatorImpl();
        carModeMediator.registerCarComponent(new Aircond());
        carModeMediator.registerCarComponent(new Gearbox());
        carModeMediator.registerCarComponent(new Steering());
        carModeMediator.registerCarComponent(new Suspension());
        carModeMediator.registerCarComponent(new Throttle());

        Command normalMode = new NormalModeCommand(carModeMediator);
        Command sportMode = new SportModeCommand(carModeMediator);
        Command ecoMode = new EcoModeCommand(carModeMediator);

        CarModeSwitch carModeSwitch = new CarModeSwitch();
        carModeSwitch.storeAndExecute(normalMode);
        System.out.println("");
        carModeSwitch.storeAndExecute(sportMode);
        System.out.println("");
        carModeSwitch.storeAndExecute(ecoMode);
    }
}
