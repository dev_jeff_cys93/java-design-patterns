package org.jc.solutions.design.patterns.behavioral.mediator;

interface CarModeMediator {
    void registerCarComponent(CarComponent carComponent);
    void ecoMode();
    void normalMode();
    void sportMode();
    void printCarComponent();
}
