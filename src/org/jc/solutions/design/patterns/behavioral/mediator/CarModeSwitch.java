package org.jc.solutions.design.patterns.behavioral.mediator;

/**
 * Invoker
 */
class CarModeSwitch {
    public void storeAndExecute(Command command) {
        command.execute();
    }
}
