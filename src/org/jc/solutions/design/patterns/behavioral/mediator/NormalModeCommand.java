package org.jc.solutions.design.patterns.behavioral.mediator;

/**
 * Concrete {@code Command}
 */
class NormalModeCommand implements Command {
    private CarModeMediator carModeMediator;

    public NormalModeCommand(CarModeMediator carModeMediator) {
        this.carModeMediator = carModeMediator;
    }

    @Override
    public void execute() {
        carModeMediator.normalMode();
        carModeMediator.printCarComponent();
    }
}