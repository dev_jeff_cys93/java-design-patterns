package org.jc.solutions.design.patterns.behavioral.mediator;

/**
 * Concrete {@code Command}
 */
class SportModeCommand implements Command {
    private CarModeMediator carModeMediator;

    public SportModeCommand(CarModeMediator carModeMediator) {
        this.carModeMediator = carModeMediator;
    }

    @Override
    public void execute() {
        carModeMediator.sportMode();
        carModeMediator.printCarComponent();
    }
}
