package org.jc.solutions.design.patterns.behavioral.mediator;

// Colleague
interface Command {
    void execute();
}
