package org.jc.solutions.design.patterns.behavioral.mediator;

abstract class CarComponent {
    public static final String CONSERVATIVE = "CONSERVATIVE";
    public static final String NORMAL = "NORMAL";
    public static final String AGGRESSIVE = "AGGRESSIVE";
    public static final String HARD = "HARD";

    protected String mode = NORMAL;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String toString() {
        return String.format("%s: %s", getClass().getSimpleName(), mode);
    }
}
