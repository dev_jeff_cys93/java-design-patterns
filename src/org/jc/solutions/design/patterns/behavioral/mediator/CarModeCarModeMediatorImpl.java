package org.jc.solutions.design.patterns.behavioral.mediator;

class CarModeCarModeMediatorImpl implements CarModeMediator {

    private CarComponent aircond;
    private CarComponent gearbox;
    private CarComponent steering;
    private CarComponent suspension;
    private CarComponent throttle;
    private String carMode;

    @Override
    public void registerCarComponent(CarComponent carComponent) {
        if (carComponent instanceof Aircond) {
            aircond = carComponent;
        } else if (carComponent instanceof Gearbox) {
            gearbox = carComponent;
        } else if (carComponent instanceof Steering) {
            steering = carComponent;
        } else if (carComponent instanceof Suspension) {
            suspension = carComponent;
        } else if (carComponent instanceof Throttle) {
            throttle = carComponent;
        }
    }

    @Override
    public void ecoMode() {
        carMode = "Eco Mode";
        if (aircond != null) {
            aircond.setMode(CarComponent.CONSERVATIVE);
        }

        if (gearbox != null) {
            gearbox.setMode(CarComponent.CONSERVATIVE);
        }

        if (steering != null) {
            steering.setMode(CarComponent.NORMAL);
        }

        if (suspension != null) {
            suspension.setMode(CarComponent.NORMAL);
        }

        if (throttle != null) {
            throttle.setMode(CarComponent.CONSERVATIVE);
        }
    }

    @Override
    public void normalMode() {
        carMode = "Normal Mode";
        if (aircond != null) {
            aircond.setMode(CarComponent.NORMAL);
        }

        if (gearbox != null) {
            gearbox.setMode(CarComponent.NORMAL);
        }

        if (steering != null) {
            steering.setMode(CarComponent.NORMAL);
        }

        if (suspension != null) {
            suspension.setMode(CarComponent.NORMAL);
        }

        if (throttle != null) {
            throttle.setMode(CarComponent.NORMAL);
        }
    }

    @Override
    public void sportMode() {
        carMode = "Sport Mode";
        if (aircond != null) {
            aircond.setMode(CarComponent.NORMAL);
        }

        if (gearbox != null) {
            gearbox.setMode(CarComponent.AGGRESSIVE);
        }

        if (steering != null) {
            steering.setMode(CarComponent.HARD);
        }

        if (suspension != null) {
            suspension.setMode(CarComponent.HARD);
        }

        if (throttle != null) {
            throttle.setMode(CarComponent.AGGRESSIVE);
        }
    }

    @Override
    public void printCarComponent() {
        System.out.println(carMode);
        if (aircond != null) {
            System.out.println(aircond.toString());
        }

        if (gearbox != null) {
            System.out.println(gearbox.toString());
        }

        if (steering != null) {
            System.out.println(steering.toString());
        }

        if (suspension != null) {
            System.out.println(suspension.toString());
        }

        if (throttle != null) {
            System.out.println(throttle.toString());
        }
    }
}
