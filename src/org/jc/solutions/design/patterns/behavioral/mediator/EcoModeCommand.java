package org.jc.solutions.design.patterns.behavioral.mediator;

/**
 * Concrete {@code Command}
 */
class EcoModeCommand implements Command {
    private CarModeMediator carModeMediator;

    public EcoModeCommand(CarModeMediator carModeMediator) {
        this.carModeMediator = carModeMediator;
    }

    @Override
    public void execute() {
        carModeMediator.ecoMode();
        carModeMediator.printCarComponent();
    }
}