package org.jc.solutions.design.patterns.behavioral.mediator;

interface ChatMediator {

    public void sendMessage(String msg, User user);

    void addUser(User user);
}