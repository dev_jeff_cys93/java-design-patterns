package org.jc.solutions.design.patterns.behavioral.memento;

import java.util.Date;

class OrderMomento {
    private String id;
    private String item;
    private int quantity;
    private Date createDate;
    private Date lastUpdateDate;

    public OrderMomento(Order order) {
        this.id = order.getId();
        this.item = order.getItem();
        this.quantity = order.getQuantity();
        this.createDate = order.getCreateDate();
        this.lastUpdateDate = order.getLastUpdateDate();
    }

    public String getId() {
        return id;
    }

    public String getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }
}
