package org.jc.solutions.design.patterns.behavioral.memento;

import java.util.Stack;

class CareTaker {
    private Stack<OrderMomento> history = new Stack<>();

    public void save(Order order) {
        history.push(order.save());
    }

    public void revert(Order order) {
        order.revert(history.pop());
    }
}
