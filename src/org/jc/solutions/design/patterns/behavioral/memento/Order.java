package org.jc.solutions.design.patterns.behavioral.memento;

import java.util.Date;

class Order {
    private String id;
    private String item;
    private int quantity;
    private Date createDate;
    private Date lastUpdateDate;

    public Order(String id, String item, int quantity) {
        this.id = id;
        this.item = item;
        this.quantity = quantity;

        Date today = new Date();
        this.createDate = today;
        this.lastUpdateDate = today;
    }

    public String getId() {
        return id;
    }

    public String getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void updateItem(String item) {
        this.item = item;
        this.lastUpdateDate = new Date();
    }

    public void updateQuantity(int quantity) {
        this.quantity = quantity;
        this.lastUpdateDate = new Date();
    }

    public OrderMomento save() {
        return new OrderMomento(this);
    }

    public void revert(OrderMomento orderMomento) {
        this.id = orderMomento.getId();
        this.item = orderMomento.getItem();
        this.quantity = orderMomento.getQuantity();
        this.createDate = orderMomento.getCreateDate();
        this.lastUpdateDate = orderMomento.getLastUpdateDate();
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", item='" + item + '\'' +
                ", quantity=" + quantity +
                ", createDate=" + createDate +
                ", lastUpdateDate=" + lastUpdateDate +
                '}';
    }
}
