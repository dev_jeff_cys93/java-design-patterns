package org.jc.solutions.design.patterns.behavioral.memento;

import java.util.Arrays;

public class MementoMain {
    public static void main(String[] args) {
        try {
            CareTaker careTaker = new CareTaker();
            Order order = new Order("0001", "Macbook Air", 1);
            careTaker.save(order);
            System.out.println("Initial order - " + order);

            Thread.sleep(1000);
            order.updateQuantity(3);
            careTaker.save(order);
            System.out.println("Updated quantity and saved - " + order);

            Thread.sleep(1000);
            order.updateItem("Macbook Pro");
            System.out.println("Updated quantity and not saved - " + order);

            careTaker.revert(order);
            System.out.println("Revert#1 - " + order);

            careTaker.revert(order);
            System.out.println("Revert#2 - " + order);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
