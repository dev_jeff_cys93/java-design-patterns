package org.jc.solutions.design.patterns.behavioral.interpreter;

interface Expression {
    boolean interpret(Person person);
}
