package org.jc.solutions.design.patterns.behavioral.interpreter;

import java.util.function.Predicate;

class TerminalExpression implements Expression {
    private Predicate<Person> predicate;

    public TerminalExpression(Predicate<Person> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean interpret(Person person) {
        return predicate.test(person);
    }
}
