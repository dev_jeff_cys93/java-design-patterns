package org.jc.solutions.design.patterns.behavioral.interpreter;

class Person {
    private final String name;
    private final double income;

    public Person(String name, double income) {
        this.name = name;
        this.income = income;
    }

    public String getName() {
        return name;
    }

    public double getIncome() {
        return income;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", income=" + income +
                '}';
    }
}
