package org.jc.solutions.design.patterns.behavioral.interpreter;

class OrExpression implements Expression {
    private final TerminalExpression terminalExpression1;
    private final TerminalExpression terminalExpression2;

    public OrExpression(TerminalExpression terminalExpression1, TerminalExpression terminalExpression2) {
        this.terminalExpression1 = terminalExpression1;
        this.terminalExpression2 = terminalExpression2;
    }

    @Override
    public boolean interpret(Person person) {
        return terminalExpression1.interpret(person) || terminalExpression2.interpret(person);
    }
}
