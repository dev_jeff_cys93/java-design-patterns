package org.jc.solutions.design.patterns.behavioral.interpreter;

public class InterpreterMain {
    private static final Expression expressionB40;
    private static final Expression expressionM40;
    private static final Expression expressionT20;

    static {
        expressionB40 = new TerminalExpression(p -> p.getIncome() < 4360);
        expressionM40 = new AndExpression(new TerminalExpression(p -> p.getIncome() >= 4360), new TerminalExpression(p -> p.getIncome() < 9619));
        expressionT20 = new TerminalExpression(p -> p.getIncome() >= 9619);
    }

    private static String getIncomeGroup(Person person) {
        if (expressionB40.interpret(person)) {
            return "B40";
        }

        if (expressionM40.interpret(person)) {
            return "M40";
        }

        if (expressionT20.interpret(person)) {
            return "T20";
        }

        return "";
    }

    public static void main(String[] args) {
        Person personA = new Person("Person A", 50000);

        System.out.println(personA.toString());
        System.out.println("Income Group: " + getIncomeGroup(personA));
    }
}
