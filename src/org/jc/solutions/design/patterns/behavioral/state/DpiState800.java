package org.jc.solutions.design.patterns.behavioral.state;

public class DpiState800 extends State {
    private Mouse mouse;

    public DpiState800(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    void handleRequest() {
        mouse.setState(mouse.getDpiState1600());
    }

    @Override
    public String toString() {
        return "800";
    }
}
