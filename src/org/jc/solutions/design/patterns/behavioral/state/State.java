package org.jc.solutions.design.patterns.behavioral.state;

abstract class State {
    abstract void handleRequest();
}
