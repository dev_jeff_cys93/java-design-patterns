package org.jc.solutions.design.patterns.behavioral.state;

public class DpiState3200 extends State {
    private Mouse mouse;

    public DpiState3200(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    void handleRequest() {
        mouse.setState(mouse.getDpiState6400());
    }

    @Override
    public String toString() {
        return "3200";
    }
}
