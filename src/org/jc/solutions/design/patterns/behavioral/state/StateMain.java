package org.jc.solutions.design.patterns.behavioral.state;

public class StateMain {
    public static void main(String[] args) {
        Mouse mouse = new Mouse();
        mouse.toggleDpi();
        mouse.toggleDpi();
        mouse.toggleDpi();
        mouse.toggleDpi();
        mouse.toggleDpi();
        mouse.toggleDpi();
    }
}
