package org.jc.solutions.design.patterns.behavioral.state;

public class Mouse {
    private State dpiState800;
    private State dpiState1600;
    private State dpiState3200;
    private State dpiState6400;
    private State state;

    public Mouse() {
        this.dpiState800 = new DpiState800(this);
        this.dpiState1600 = new DpiState1600(this);
        this.dpiState3200 = new DpiState3200(this);
        this.dpiState6400 = new DpiState6400(this);
        setState(dpiState800);
    }

    public void setState(State state) {
        this.state = state;
        System.out.println("Mouse DPI: " + state);
    }

    public State getDpiState800() {
        return dpiState800;
    }

    public State getDpiState1600() {
        return dpiState1600;
    }

    public State getDpiState3200() {
        return dpiState3200;
    }

    public State getDpiState6400() {
        return dpiState6400;
    }

    public void toggleDpi() {
        state.handleRequest();
    }
}
