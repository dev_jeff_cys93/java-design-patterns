package org.jc.solutions.design.patterns.behavioral.state;

public class DpiState6400 extends State {
    private Mouse mouse;

    public DpiState6400(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    void handleRequest() {
        mouse.setState(mouse.getDpiState800());
    }

    @Override
    public String toString() {
        return "6400";
    }
}
