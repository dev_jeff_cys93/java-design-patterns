package org.jc.solutions.design.patterns.behavioral.state;

public class DpiState1600 extends State {
    private Mouse mouse;

    public DpiState1600(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    void handleRequest() {
        mouse.setState(mouse.getDpiState3200());
    }

    @Override
    public String toString() {
        return "1600";
    }
}
