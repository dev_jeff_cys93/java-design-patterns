package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

public class ChainOfResponsibilityMain {
    public static void main(String[] arg) {
        Logger.info("this is info.");
        Logger.all("this is all.");
        Logger.debug("this is debug.");
        Logger.warn("this is warn.");
        Logger.error("this is error.");
    }
}
