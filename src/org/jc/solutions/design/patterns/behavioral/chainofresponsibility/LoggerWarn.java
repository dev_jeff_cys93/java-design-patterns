package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

class LoggerWarn extends Handler {
    @Override
    void printLog(Log log) {
        if (log.getLoggerLevel() == LoggerLevel.WARN) {
            System.out.println(String.format(Log.LOG_FORMAT, log.getLoggerLevel().getLoggerPrefix(), log.getMessage()));
        } else {
            successor.printLog(log);
        }
    }
}
