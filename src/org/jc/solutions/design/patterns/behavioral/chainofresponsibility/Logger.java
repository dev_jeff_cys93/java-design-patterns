package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

class Logger {
    private static final LoggerInfo loggerInfo = new LoggerInfo();
    private static final LoggerDebug loggerDebug = new LoggerDebug();
    private static final LoggerWarn loggerWarn = new LoggerWarn();
    private static final LoggerError loggerError = new LoggerError();
    private static final LoggerAll loggerAll = new LoggerAll();

    static {
        loggerError.setSuccessor(loggerWarn);
        loggerWarn.setSuccessor(loggerInfo);
        loggerInfo.setSuccessor(loggerDebug);
        loggerDebug.setSuccessor(loggerAll);
    }

    public static void error(String message) {
        loggerError.printLog(new Log(LoggerLevel.ERROR, message));
    }

    public static void warn(String message) {
//        loggerWarn.printLog(new Log(LoggerLevel.WARN, message));
        loggerError.printLog(new Log(LoggerLevel.WARN, message)); // To test the chain
    }

    public static void info(String message) {
//        loggerInfo.printLog(new Log(LoggerLevel.INFO, message));
        loggerError.printLog(new Log(LoggerLevel.INFO, message)); // To test the chain
    }

    public static void debug(String message) {
//        loggerDebug.printLog(new Log(LoggerLevel.DEBUG, message));
        loggerError.printLog(new Log(LoggerLevel.DEBUG, message)); // To test the chain
    }

    public static void all(String message) {
//        loggerAll.printLog(new Log(LoggerLevel.ALL, message));
        loggerError.printLog(new Log(LoggerLevel.ALL, message)); // To test the chain
    }
}
