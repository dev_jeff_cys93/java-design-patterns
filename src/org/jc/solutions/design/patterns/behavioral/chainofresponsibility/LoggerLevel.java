package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

enum LoggerLevel {
    DEBUG("[DEBUG]"),
    INFO("[INFO]"),
    WARN("[WARN]"),
    ERROR("[ERROR]"),
    ALL("[ALL]");

    private String loggerPrefix;

    LoggerLevel(String loggerPrefix) {
        this.loggerPrefix = loggerPrefix;
    }

    public String getLoggerPrefix() {
        return loggerPrefix;
    }
}
