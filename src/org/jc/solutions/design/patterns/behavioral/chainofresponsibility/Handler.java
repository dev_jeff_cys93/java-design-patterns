package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

abstract class Handler {
    protected Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    abstract void printLog(Log log);
}
