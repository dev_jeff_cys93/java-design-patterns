package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

import java.util.logging.Logger;

class Log {
    private LoggerLevel loggerLevel;
    private String message;

    public static final String LOG_FORMAT = "%s: %s";

    public Log(String message) {
        this.message = message;
        this.loggerLevel = LoggerLevel.INFO;
    }

    public Log(LoggerLevel loggerLevel, String message) {
        this.loggerLevel = loggerLevel;
        this.message = message;
    }

    public LoggerLevel getLoggerLevel() {
        return loggerLevel;
    }

    public void setLoggerLevel(LoggerLevel loggerLevel) {
        this.loggerLevel = loggerLevel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
