package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

class LoggerError extends Handler {
    @Override
    void printLog(Log log) {
        if (log.getLoggerLevel() == LoggerLevel.ERROR) {
            System.out.println(String.format(Log.LOG_FORMAT, log.getLoggerLevel().getLoggerPrefix(), log.getMessage()));
        } else {
            successor.printLog(log);
        }
    }
}
