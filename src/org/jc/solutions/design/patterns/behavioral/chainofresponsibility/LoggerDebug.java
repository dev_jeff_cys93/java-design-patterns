package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

class LoggerDebug extends Handler {
    @Override
    void printLog(Log log) {
        if (log.getLoggerLevel() == LoggerLevel.DEBUG) {
            System.out.println(String.format(Log.LOG_FORMAT, log.getLoggerLevel().getLoggerPrefix(), log.getMessage()));
        } else {
            successor.printLog(log);
        }
    }
}
