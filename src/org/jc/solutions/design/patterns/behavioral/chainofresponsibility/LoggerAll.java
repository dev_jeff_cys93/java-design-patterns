package org.jc.solutions.design.patterns.behavioral.chainofresponsibility;

class LoggerAll extends Handler {
    @Override
    void printLog(Log log) {
        System.out.println(String.format(Log.LOG_FORMAT, log.getLoggerLevel().getLoggerPrefix(), log.getMessage()));
    }
}
